/** Dependencies */
import {
    NodeBlock,
    Slots,
    assert,
    validator,
} from "tripetto-runner-foundation";
import "./condition";

/* tslint:disable-next-line:max-line-length */
const IS_PHONE_NUMBER =
    /^(?:(?:\(?(?:00|\+)([1-4]\d\d|[1-9]\d?)\)?)?[\-\.\ \\\/]?)?((?:\(?\d{1,}\)?[\-\.\ \\\/]?){0,})(?:[\-\.\ \\\/]?(?:#|ext\.?|extension|x)[\-\.\ \\\/]?(\d+))?$/i;

export abstract class PhoneNumber extends NodeBlock {
    /** Contains the phone number slot with the value. */
    readonly phoneNumberSlot = assert(
        this.valueOf<string, Slots.String>("phone-number")
    );

    /** Contains if the block is required. */
    readonly required = this.phoneNumberSlot.slot.required || false;

    @validator
    validate(): boolean {
        return (
            !this.phoneNumberSlot.string ||
            IS_PHONE_NUMBER.test(this.phoneNumberSlot.value)
        );
    }
}
